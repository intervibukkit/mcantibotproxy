package ru.intervi.mcantibotplugin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

import org.bukkit.Bukkit;

public class Client {
	public Client(Main main) {
		this.main = main;
	}
	
	private Main main;
	private Socket socket = null;
	Reader reader;
	private volatile boolean attack;
	private volatile int logins = 0;
	
	public void start() {
		try {
			if (socket != null && socket.isConnected()) return;
			reader = new Reader();
			reader.start();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	public void stop() {
		try {
			if (socket == null || socket.isClosed()) return;
			socket.close();
		} catch(Exception e) {e.printStackTrace();}
		finally {
			try {reader.interrupt();}
			catch(Exception e) {e.printStackTrace();}
		}
	}
	
	public boolean isAttack() {
		return attack;
	}
	
	public int getLogins() {
		return logins;
	}
	
	private class Reader extends Thread {
		@Override
		public void run() {
			int attempts = 0;
			try {socket = new Socket(main.conf.host, main.conf.port);}
			catch(Exception e) {
				e.printStackTrace();
				return;
			}
			while(true) {
				try {
					if (!main.conf.enabled) {
						socket.close();
						break;
					}
					BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					while(true) {
						String line = reader.readLine();
						if (line == null) {
							Thread.sleep(1000);
							continue;
						}
						line = line.trim();
						boolean isAttack = Boolean.parseBoolean(line.substring(0, line.indexOf(',')));
						logins = Integer.parseInt(line.substring(line.indexOf(',')+1));
						if (isAttack) {
							if (isAttack != attack) {
								if (main.conf.notify) {
									Bukkit.broadcast(main.conf.startAttack, "mcabpp.receive");
									Bukkit.broadcast(main.conf.bps.replaceAll("%bots%", String.valueOf(logins)), "mcabpp.receive");
								}
								if (main.conf.log) {
									main.getLogger().warning(main.conf.startAttack);
									main.getLogger().warning(main.conf.bps.replaceAll("%bots%", String.valueOf(logins)));
								}
							} else if (main.conf.notifyAll) {
								Bukkit.broadcast(main.conf.bps.replaceAll("%bots%", String.valueOf(logins)), "mcabpp.receive");
								if (main.conf.log) main.getLogger().warning(main.conf.bps.replaceAll("%bots%", String.valueOf(logins)));
							}
						} else {
							if (isAttack != attack) {
								if (main.conf.notify) Bukkit.broadcast(main.conf.stopAttack, "mcabpp.receive");
								if (main.conf.log) main.getLogger().warning(main.conf.stopAttack);
							}
						}
						attack = isAttack;
					}
				} catch(Exception e) {
					e.printStackTrace();
					if (attempts >= main.conf.rcAttempts) break;
					try {
						socket = new Socket(main.conf.host, main.conf.port);
						attempts++;
					} catch(Exception e2) {
						e2.printStackTrace();
						try {Thread.sleep(1000);}
						catch(Exception e3) {e3.printStackTrace();}
					}
				}
			}
		}
	}
}