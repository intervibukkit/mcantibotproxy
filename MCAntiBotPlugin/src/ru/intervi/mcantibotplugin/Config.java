package ru.intervi.mcantibotplugin;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import net.md_5.bungee.api.ChatColor;

public class Config {
	public Config(Main main) {
		this.main = main;
	}
	
	private Main main;
	
	public boolean enabled = true;
	public String host = "127.0.0.1";
	public int port = 30910;
	public boolean notify = true;
	public boolean notifyAll = false;
	public boolean log = true;
	public int rcAttempts = 4;
	
	public String startAttack = "&cначалась бот-атака";
	public String stopAttack = "&cбот-атака закончилась";
	public String bps = "&c%bots% в секунду";
	public List<String> help = null;
	public List<String> info = null;
	public String reload = "&eконфиг перезагружен";
	public String noperm = "&cнет прав";
	public String start = "&eподключение...";
	public String stop = "&eотключение...";
	
	public static String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	public static List<String> color(List<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) result.add(color(s));
		return result;
	}
	
	public void load() {
		main.saveDefaultConfig();
		main.reloadConfig();
		FileConfiguration conf = main.getConfig();
		enabled = conf.getBoolean("enabled");
		host = conf.getString("host");
		port = conf.getInt("port");
		notify = conf.getBoolean("notify");
		notifyAll = conf.getBoolean("notify-all");
		log = conf.getBoolean("log");
		rcAttempts = conf.getInt("rc-attempts");
		startAttack = color(conf.getString("start-attack"));
		stopAttack = color(conf.getString("stop-attack"));
		bps = color(conf.getString("bps"));
		help = color(conf.getStringList("help"));
		info = color(conf.getStringList("info"));
		reload = color(conf.getString("reload"));
		noperm = color(conf.getString("noperm"));
		start = color(conf.getString("start"));
		stop = color(conf.getString("stop"));
	}
}