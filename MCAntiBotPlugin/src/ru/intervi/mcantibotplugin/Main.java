package ru.intervi.mcantibotplugin;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	Config conf = new Config(this);
	Client client = new Client(this);
	
	@Override
	public void onEnable() {
		try {
			conf.load();
			if (conf.enabled) client.start();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	@Override
	public void onDisable() {
		try {
			client.stop();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("mcabpp.cmds")) {
			sender.sendMessage(conf.noperm);
			return true;
		}
		if (args == null || args.length == 0) {
			sender.sendMessage(conf.help.toArray(new String[conf.help.size()]));
			return true;
		}
		switch(args[0].toLowerCase()) {
		case "reload":
			conf.load();
			sender.sendMessage(conf.reload);
			break;
		case "start":
			client.start();
			sender.sendMessage(conf.start);
			break;
		case "stop":
			client.stop();
			sender.sendMessage(conf.stop);
			break;
		case "help":
			sender.sendMessage(conf.help.toArray(new String[conf.help.size()]));
			break;
		case "info":
			String[] info = new String[conf.info.size()];
			for (int i = 0; i < info.length; i++) info[i] = conf.info.get(i).replaceAll("%attack%", String.valueOf(client.isAttack())).replaceAll("%bots%", String.valueOf(client.getLogins()));
			sender.sendMessage(info);
			break;
		default:
			return false;
		}
		return true;
	}
}