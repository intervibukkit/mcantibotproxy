package ru.intervi.mcantibotproxy.filter;

import com.github.steveice10.mc.protocol.data.message.TextMessage;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerChatPacket;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.event.session.SessionListener;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class Filter {
	public Filter(FakeServer server) {
		this.server = server;
		cache = new Cache(this.server);
	}
	
	private volatile FakeServer server;
	private volatile Cache cache;
	
	public FilterData getData(Session session) {
		FilterData result = cache.get();
		session.setFlag("request", result.KEY);
		return result;
	}
	
	public Listener getListener() {
		return new Listener();
	}
	
	public class Listener extends SessionAdapter {
		@Override
		public void packetReceived(PacketReceivedEvent event) {
			try {
				if (event.getPacket() instanceof ClientChatPacket) {
					if (!event.getSession().hasFlag("request")) return;
					String message = ((ClientChatPacket) event.getPacket()).getMessage().trim().toLowerCase();
					message = message.replace(')', ' ');
					message = message.trim();
					if (!((String) event.getSession().getFlag("request")).equals(message)) {
						event.getSession().send(new ServerChatPacket(new TextMessage(server.main.conf.wrongMsg)));
						int wrong = event.getSession().hasFlag("wrong") ? ((Integer) event.getSession().getFlag("wrong")).intValue() + 1 : 1;
						if (wrong > server.main.conf.wrongAttempt) {
							server.banWrong(event.getSession());
						}
					} else {
						for (SessionListener listener : event.getSession().getListeners()) {
							if (listener instanceof Listener) {
								event.getSession().removeListener(listener);
								break;
							}
						}
						event.getSession().send(new ServerChatPacket(new TextMessage(server.main.conf.respawn)));
						server.putToProxy(event.getSession(), true);
						if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " checked, put to proxy");
					}
				}
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			}
		}
	}
	
	public void serverClosed() {
		try {cache.clear();}
		catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public void serverStart() {
		try {cache.fill();}
		catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public synchronized void updateCache() {
		try {cache.update();}
		catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
}