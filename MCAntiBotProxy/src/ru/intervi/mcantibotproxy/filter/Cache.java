package ru.intervi.mcantibotproxy.filter;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class Cache {
	public Cache(FakeServer server) {
		this.server = server;
		fill();
	}
	
	private FakeServer server;
	private volatile ArrayList<FilterData> list = new ArrayList<FilterData>();
	private final Color[] COLORS = {
			Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA,
			Color.ORANGE, Color.PINK, Color.RED, Color.WHITE, Color.YELLOW
	};
	
	private class Filler extends Thread {
		@Override
		public void run() {
			try {while(list.size() < server.main.conf.cache) list.add(getData());}
			catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			}
		}
	}
	
	public synchronized FilterData get() {
		if (list.isEmpty()) return null;
		try {
			if (server.main.conf.cacheFillStart > 0) return list.get(0);
			else return list.get(new Random().nextInt(list.size()));
		} finally {
			if (server.main.conf.cacheFillStart > 0) {
				list.remove(0);
				if (list.size() <= server.main.conf.cacheFillStart) fill();
			}
		}
	}
	
	private FilterData getImage() {
		try {
			Random random = new Random();
			int keyind = random.nextInt(server.main.images.getKeys().size());
			String ikey = server.main.images.getKeys().get(keyind);
			String keys[] = ikey.split(",");
			String key = keys[random.nextInt(keys.length)];
			String text[] = new String[server.main.conf.varriants+1];
			text[0] = server.main.conf.messTitle;
			int line = random.nextInt(text.length);
			if (line == 0) line = 1;
			for (int i = 1; i < text.length; i++) {
				if (i == line) {
					text[i] = String.valueOf(i) + ") " + key;
					continue;
				}
				int rnd = keyind;
				while(rnd == keyind) rnd = random.nextInt(server.main.images.getKeys().size());
				String wkeys[] = server.main.images.getKeys().get(rnd).split(",");
				text[i] = String.valueOf(i) + ") " + wkeys[new Random().nextInt(wkeys.length)];
			}
			String strKey = String.valueOf(line);
			return new FilterData(server.main.images.get(ikey), text, strKey);
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
			return null;
		}
	}
	
	private FilterData getCaptcha() {
		try {
			Random random = new Random();
			Font font = new Font(server.main.conf.font, Font.BOLD, server.main.conf.fontSize);
			int width = 128;
			int height = 128;
			Color[] colors = {Color.WHITE, Color.WHITE, Color.WHITE};
			for (byte i = 0; i < colors.length; i++) {
				Color c = null;
				do c = COLORS[random.nextInt(COLORS.length)];
				while(c.equals(colors[0]) || c.equals(colors[1]) || c.equals(colors[2]));
				colors[i] = c;
			}
			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = image.createGraphics();
			g2d.setFont(font);
			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g2d.setRenderingHints(rh);
			GradientPaint gp = new GradientPaint(0, 0, colors[0], 0, height/2, colors[1], true);
			g2d.setPaint(gp);
			g2d.fillRect(0, 0, width, height);
			g2d.setColor(colors[2]);
			int x = 10 + (Math.abs(random.nextInt(width)) % 15);
			int y = 20 + Math.abs(random.nextInt(height)) % 45;
			String text = "";
			while(text.length() < server.main.conf.captchaLength) text += server.main.conf.captchaSymbols.charAt(random.nextInt(server.main.conf.captchaSymbols.length()));
			g2d.drawString(text, x, y);
			g2d.dispose();
			return new FilterData(new Map().drawImage(0, 0, image), new String[]{server.main.conf.captchaTitle}, text);
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
			return null;
		}
	}
	
	private FilterData getData() {
		if (server.main.conf.captcha) return getCaptcha();
		else return getImage();
	}
	
	public synchronized void fill() {
		try {new Filler().start();}
		catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public synchronized void clear() {
		try {list.clear();}
		catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public synchronized void update() {
		try {
			clear();
			fill();
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
}