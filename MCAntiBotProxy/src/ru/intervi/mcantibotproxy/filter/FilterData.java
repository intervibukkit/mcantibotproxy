package ru.intervi.mcantibotproxy.filter;

public class FilterData {
	public FilterData(byte[] map, String[] text, String key) {
		MAP = map;
		TEXT = text;
		KEY = key;
	}
	
	public final byte[] MAP;
	public final String[] TEXT;
	public final String KEY;
}