package ru.intervi.mcantibotproxy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.net.Proxy;
import java.util.Properties;

public class Config {
	public Config(Main main) {
		this.main = main;
		load();
	}
	
	private Main main;
	
	public boolean debug = true;
	public boolean logging = false;
	
	public String serverHost = "127.0.0.1";
	public int serverPort = 25566;
	public int maxAttempt = 10;
	
	public boolean onlineMode = false;
	public String host = "127.0.0.1";
	public int port = 25565;
	public String serverId = "";
	public Proxy proxy = Proxy.NO_PROXY;
	public Proxy authProxy = Proxy.NO_PROXY;
	public int statusPingMS = 1000;
	public int sessionLimit = 200000;
	public String kickLimit = "сервер заполнен";
	public int compression = 256;
	
	public boolean forceCheck = true;
	
	public boolean captcha = true;
	public String captchaTitle = "ВВЕДИТЕ КАПЧУ";
	public String font = "Liberation Sans";
	public int fontSize = 12;
	public int captchaLength = 4;
	public String captchaSymbols = "qwertyuiopasdfghjklzxcvbnm1234567890";
	
	public int loginLimit = 3;
	public int loginMS = 1000;
	public int waitMS = 30000;
	public int normalReadTimeout = 30;
	public int attackReadTimeout = 5;
	public int normalWriteTimeout = 30;
	public int attackWriteTimeout = 5;
	public int ipLimit = 3;
	public String ipMsg = "слишком много подключений";
	public int timeLimit = 20;
	public int tlClearMS = 1000;
	public String timeoutMsg = "время вышло";
	public String messTitle = "Введите НОМЕР ответа:";
	
	public int cache = 30000;
	public int cacheFillStart = 1000;
	
	public int wrongAttempt = 2;
	public int varriants = 6;
	public boolean ban = false;
	public String bancmd = "iptables -I INPUT -s %host% -j DROP";
	public String wrongMsg = "НЕ ПРАВИЛЬНО!";
	public String kickMsg = "не правильно!";
	public String banMsg = "ты забанен на веки";
	public String respawn = "перемещение на сервер...";
	
	public boolean statServer = true;
	public String statHost = "127.0.0.1";
	public int statPort = 30910;
	public int statLimit = 20;
	
	public static boolean saveFile(InputStream stream, File file) { //сохранить поток в файл
		if (stream == null || file == null) return false;
		try {
			FileOutputStream fos = new FileOutputStream(file);
			byte[] buff = new byte[65536];
			int n;
			while((n = stream.read(buff)) > 0){
				fos.write(buff, 0, n);
				fos.flush();
			}
			fos.close();
			return true;
		} catch(Exception e) {
			return false;
		}
}
	
	public void load() {
		try {
			File file = new File(main.PATH.getAbsolutePath() + File.separatorChar + "settings.properties");
			if (!file.isFile()) saveFile(this.getClass().getResourceAsStream("/settings.properties"), file);
			Properties prop = new Properties();
			prop.load(new FileReader(file));
			debug = Boolean.parseBoolean(prop.getProperty("debug"));
			logging = Boolean.parseBoolean(prop.getProperty("logging"));
			serverHost = prop.getProperty("serverHost");
			serverPort = Integer.parseInt(prop.getProperty("serverPort"));
			onlineMode = Boolean.parseBoolean("onlineMode");
			host = prop.getProperty("host");
			port = Integer.parseInt(prop.getProperty("port"));
			statusPingMS = Integer.parseInt(prop.getProperty("statusPingMS"));
			forceCheck = Boolean.parseBoolean(prop.getProperty("forceCheck"));
			loginLimit = Integer.parseInt(prop.getProperty("loginLimit"));
			loginMS = Integer.parseInt(prop.getProperty("loginMS"));
			waitMS = Integer.parseInt(prop.getProperty("waitMS"));
			normalReadTimeout = Integer.parseInt(prop.getProperty("normalReadTimeout"));
			normalWriteTimeout = Integer.parseInt(prop.getProperty("normalWriteTimeout"));
			attackReadTimeout = Integer.parseInt(prop.getProperty("attackReadTimeout"));
			attackWriteTimeout = Integer.parseInt(prop.getProperty("attackWriteTimeout"));
			ipLimit = Integer.parseInt(prop.getProperty("ipLimit"));
			ipMsg = prop.getProperty("ipMsg");
			timeLimit = Integer.parseInt(prop.getProperty("timeLimit"));
			tlClearMS = Integer.parseInt(prop.getProperty("tlClearMS"));
			timeoutMsg = prop.getProperty("timeoutMsg");
			messTitle = prop.getProperty("messTitle");
			wrongAttempt = Integer.parseInt(prop.getProperty("wrongAttempt"));
			varriants = Integer.parseInt(prop.getProperty("varriants"));
			ban = Boolean.parseBoolean(prop.getProperty("ban"));
			bancmd = prop.getProperty("bancmd");
			wrongMsg = prop.getProperty("wrongMsg");
			kickMsg = prop.getProperty("kickMsg");
			banMsg = prop.getProperty("banMsg");
			sessionLimit = Integer.parseInt(prop.getProperty("sessionLimit"));
			cache = Integer.parseInt(prop.getProperty("cache"));
			cacheFillStart = Integer.parseInt(prop.getProperty("cacheFillStart"));
			kickLimit = prop.getProperty("kickLimit");
			serverId = prop.getProperty("serverId");
			respawn = prop.getProperty("respawn");
			compression = Integer.parseInt(prop.getProperty("compression"));
			captcha = Boolean.parseBoolean(prop.getProperty("captcha"));
			fontSize = Integer.parseInt(prop.getProperty("fontSize"));
			captchaTitle = prop.getProperty("captchaTitle");
			captchaLength = Integer.parseInt(prop.getProperty("captchaLength"));
			captchaSymbols = prop.getProperty("captchaSymbols");
			font = prop.getProperty("font");
			maxAttempt = Integer.parseInt(prop.getProperty("maxAttempt"));
			statServer = Boolean.parseBoolean(prop.getProperty("statServer"));
			statHost = prop.getProperty("statHost");
			statPort = Integer.parseInt(prop.getProperty("statPort"));
			statLimit = Integer.parseInt(prop.getProperty("statLimit"));
		} catch(Exception e) {e.printStackTrace();}
	}
}