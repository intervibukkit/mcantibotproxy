package ru.intervi.mcantibotproxy.stat;

import java.util.Timer;
import java.util.TimerTask;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class Stat {
	public Stat(FakeServer server) {
		this.server = server;
		sockServer = new Server(this.server);
		startTimer();
	}
	
	private FakeServer server;
	private volatile int logins = 0;
	private volatile boolean attack = false;
	private Timer timer = null;
	private Server sockServer;
	
	public synchronized void login() {
		logins++;
	}
	
	public synchronized void setAttack(boolean attack) {
		this.attack = attack;
	}
	
	public void startTimer() {
		timer = new Timer();
		timer.schedule(new Printer(), 1000, 1000);
	}
	
	public void serverClosed() {
		try {
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
			sockServer.serverClosed();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	private class Printer extends TimerTask {
		@Override
		public void run() {
			try {
				if (logins != 0) {
					if (attack) System.out.println("bots per second: " + logins);
					else System.out.println("sessions adding per second: " + logins);
				}
				sockServer.sendInfo(String.valueOf(attack) + ',' + String.valueOf(logins));
				logins = 0;
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			}
		}
	}
}