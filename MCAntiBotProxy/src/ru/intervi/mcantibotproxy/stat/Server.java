package ru.intervi.mcantibotproxy.stat;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class Server {
	public Server(FakeServer server) {
		this.server = server;
		if (server.main.conf.statServer) {
			try {
				sockServer = new ServerSocket(server.main.conf.statPort, server.main.conf.statLimit, InetAddress.getByName(server.main.conf.statHost));
				adder = new Adder();
				adder.start();
			} catch(Exception e) {e.printStackTrace();}
		}
	}
	
	private FakeServer server;
	private ServerSocket sockServer = null;
	private Adder adder;
	private HashMap<Socket, BufferedWriter> clients = new HashMap<Socket, BufferedWriter>();
	
	public void sendInfo(String info) {
		ArrayList<Socket> remove = new ArrayList<Socket>();
		for (Entry<Socket, BufferedWriter> entry : clients.entrySet()) {
			try {
				BufferedWriter writer = entry.getValue();
				writer.write(info);
				writer.newLine();
				writer.flush();
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
				remove.add(entry.getKey());
			}
		}
		for (Socket sock : remove) {
			try {
				sock.close();
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			} finally {clients.remove(sock);}
		}
	}
	
	public void serverClosed() {
		try {
			if (sockServer == null) return;
			sockServer.close();
		} catch(Exception e) {e.printStackTrace();}
		finally {
			try {adder.interrupt();}
			catch(Exception e) {e.printStackTrace();}
		}
	}
	
	private class Adder extends Thread {
		@Override
		public void run() {
			while(!sockServer.isClosed()) {
				try {
					Socket socket = sockServer.accept();
					if (clients.size() >= server.main.conf.statLimit) {
						socket.close();
						continue;
					}
					clients.put(socket, new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
				} catch(Exception e) {
					if (server.main.conf.debug) e.printStackTrace();
				}
			}
		}
	}
}