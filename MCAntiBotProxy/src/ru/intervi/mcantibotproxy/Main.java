package ru.intervi.mcantibotproxy;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import ru.intervi.mcantibotproxy.server.FakeServer;


/**
 * @author InterVi<intervionly@gmain.con>
 * при помощи alexandrage, Leymooo, ensirius, xDark и других
 */
public class Main {
	public Main() {
		conf = new Config(this);
		images = new Images(this);
	}
	
	public volatile Config conf;
	public volatile Images images;
	public final File PATH = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParentFile();
	private static Logger logger = Logger.getLogger("MCAntiBotProxy");
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		try {
			Main main = new Main();
			ConsoleHandler chandler = new ConsoleHandler();
			chandler.setFormatter(new LogFormatter());
			logger.addHandler(chandler);
			logger.setUseParentHandlers(false);
			System.setErr(new PrintStream(new LogOut(true), true));
			System.setOut(new PrintStream(new LogOut(false), true));
			if (main.conf.logging) {
				try {
					FileHandler fhandler = new FileHandler(main.PATH.getAbsolutePath() + File.separatorChar + "mcantibotproxy.log", true);
					fhandler.setFormatter(new LogFormatter());
					logger.addHandler(fhandler);
				} catch(Exception e) {e.printStackTrace();}
			}
			System.out.println("============================================");
			System.out.println("---------------- RUN MCAntiBotProxy");
			System.out.println("============================================");
			FakeServer server = new FakeServer(main);
			server.start();
			while(true) { try {
				String mess = in.nextLine().trim();
				if (mess == null || mess.isEmpty()) continue;
				switch(mess.toLowerCase()) {
				case "reload":
					main.conf.load();
					main.images.load();
					System.out.println("settings and images reloaded");
					break;
				case "stop":
					server.stop();
					System.out.println("server stopped");
					break;
				case "start":
					server.start();
					System.out.println("server started");
					break;
				case "exit":
					server.stop();
					System.out.println("will exiting...");
					System.exit(0);
					break;
				case "recache":
					server.filter.updateCache();
					System.out.println("update cache...");
					break;
				default:
					System.out.println("reload - reload settings and images");
					System.out.println("recache - update images cache");
					System.out.println("stop - stop server");
					System.out.println("start - start server");
					System.out.println("exit - exit this program");
				}
			} catch(Exception e) {e.printStackTrace();}}
		} catch(Exception e) {e.printStackTrace();} finally {in.close();}
	}
	
	public static class LogFormatter extends Formatter {
		@Override
        public String format(LogRecord record) {
            SimpleDateFormat logTime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Calendar cal = new GregorianCalendar();
            cal.setTimeInMillis(record.getMillis());
            //[INFO] 03-12-2017 15:32:32 -> ...
            return "[" + record.getLevel() + "] " + logTime.format(cal.getTime()) + " -> " + record.getMessage() + "\n";
        }
	}
	
	public static class LogOut extends OutputStream {
		public LogOut(boolean err) {
			ERR = err;
		}
		
		private final boolean ERR;;
		private StringBuffer buf = new StringBuffer();
		private final String SEP = System.getProperty("line.separator");
		
		@Override
		public void write(int b) {
			char ch = (char) b;
			buf.append(ch);
			if (buf.substring(buf.length()-SEP.length()).equals(SEP)) {
				String s = buf.toString().trim();
				if (ERR) logger.warning(s);
				else logger.info(s);
				buf.setLength(0);
			}
		}
		
		@Override
		public void write(byte[] b) {
			for (byte by : b) {
				char ch = (char) by;
				buf.append(ch);
				if (buf.substring(buf.length()-SEP.length()).equals(SEP)) {
					String s = buf.toString().trim();
					if (ERR) logger.warning(s);
					else logger.info(s);
					buf.setLength(0);
				}
			}
		}
	}
}