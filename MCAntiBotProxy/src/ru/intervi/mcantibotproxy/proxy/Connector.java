package ru.intervi.mcantibotproxy.proxy;

import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.packetlib.Client;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.tcp.TcpSessionFactory;

import ru.intervi.mcantibotproxy.server.extend.Protocol;

public class Connector extends Thread {
	public Connector(Proxy.Listener proxy, Session session, boolean fromFilter) {
		PROXY = proxy;
		SESSION = session;
		FROMFILTER = fromFilter;
	}
	
	private final Proxy.Listener PROXY;
	private final Session SESSION;
	private final boolean FROMFILTER;
	
	@Override
	public void run() {
		try {
			if (PROXY.PROXY.server.main.conf.debug) System.out.println("creating fake client for " + SESSION.getHost());
			GameProfile profile = SESSION.getFlag(MinecraftConstants.PROFILE_KEY);
			SESSION.setFlag("debug", Boolean.valueOf(PROXY.PROXY.server.main.conf.debug));
			Protocol protocol = new Protocol(profile, ((MinecraftProtocol) SESSION.getPacketProtocol()).getAccessToken(), SESSION, PROXY.PROXY.server.main.conf.port);
			Client client = new Client(PROXY.PROXY.server.main.conf.serverHost, PROXY.PROXY.server.main.conf.serverPort, protocol, new TcpSessionFactory(PROXY.PROXY.server.main.conf.proxy));
			client.getSession().setFlag(MinecraftConstants.AUTH_PROXY_KEY, PROXY.PROXY.server.main.conf.authProxy);
			client.getSession().addListener(new MySessionListener(PROXY.PROXY.server, SESSION, FROMFILTER));
			client.getSession().setReadTimeout(PROXY.PROXY.server.main.conf.normalReadTimeout);
			client.getSession().setWriteTimeout(PROXY.PROXY.server.main.conf.normalWriteTimeout);
			client.getSession().connect();
			PROXY.PROXY.map.put(SESSION, client);
			if (SESSION.hasFlag("attempt")) SESSION.setFlag("attempt", Integer.valueOf(((Integer) SESSION.getFlag("attempt")).intValue()+1));
			else SESSION.setFlag("attempt", Integer.valueOf(1));
		} catch(Exception e) {
			if (PROXY.PROXY.server.main.conf.debug) e.printStackTrace();
		}
	}
}