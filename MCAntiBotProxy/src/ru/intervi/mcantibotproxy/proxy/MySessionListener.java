package ru.intervi.mcantibotproxy.proxy;

import com.github.steveice10.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerRespawnPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSetCompressionPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSuccessPacket;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.PacketSentEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class MySessionListener extends SessionAdapter {
	public MySessionListener(FakeServer server, Session session, boolean fromFilter) {
		SERVER = server;
		SESSION = session;
		FROMFILTER = fromFilter;
	}
	
	private final FakeServer SERVER;
	private final Session SESSION;
	private final boolean FROMFILTER;
	private boolean respawned = false;
	
	@Override
	public void packetReceived(PacketReceivedEvent event) {
		try {
			if (SERVER.main.conf.debug) {
				System.out.println(event.getSession().getHost() + " fake client packet received");
				System.out.println("-- packet: " + event.getPacket().toString());
			}
			if (event.getPacket() instanceof LoginSetCompressionPacket) return;
			if (FROMFILTER) {
				if (event.getPacket() instanceof ServerJoinGamePacket) {
					ServerJoinGamePacket packet = event.getPacket();
					SESSION.send(new ServerRespawnPacket(packet.getDimension(), packet.getDifficulty(), packet.getGameMode(), packet.getWorldType()));
					respawned = true;
					SESSION.setFlag("authed", Boolean.valueOf(true));
					return;
				} else if (event.getPacket() instanceof LoginSuccessPacket) return;
				else if (!respawned) return;
			} else if (event.getPacket() instanceof ServerJoinGamePacket) SESSION.setFlag("authed", Boolean.valueOf(true));
			if (!SESSION.isConnected()) {
				event.getSession().disconnect("fake client for " + SESSION.getHost() + " not connected");
				return;
			}
			SESSION.send(event.getPacket());
		} catch(Exception e) {
			if (SERVER.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void packetSent(PacketSentEvent event) {
		try {
			if (SERVER.main.conf.debug) {
				System.out.println(event.getSession().getHost() + " fake client packet sent");
				System.out.println("-- packet: " + event.getPacket().toString());
			}
		} catch(Exception e) {
			if (SERVER.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void disconnected(DisconnectedEvent event) { //костыль
		try {
			if (!SESSION.isConnected() || SERVER.pinger.status == null || SESSION.hasFlag("authed")) {
				if (SESSION.isConnected()) SESSION.disconnect(event.getReason());
				if (SERVER.main.conf.debug) System.out.println("disconnected " + SESSION.getHost() + " from server: " + event.getReason());
				return;
			}
			if (SESSION.hasFlag("attempt") && ((Integer) SESSION.getFlag("attempt")).intValue() >= SERVER.main.conf.maxAttempt) return;
			if (SERVER.main.conf.debug) System.out.println("disconnected, reconnect (" + event.getReason() + ')');
			new Connector(SERVER.proxy.getListener(), SESSION, FROMFILTER).start();
		} catch(Exception e) {
			if (SERVER.main.conf.debug) e.printStackTrace();
		}
	}
}