package ru.intervi.mcantibotproxy.proxy;

import java.util.HashMap;

import com.github.steveice10.mc.protocol.packet.handshake.client.HandshakePacket;
import com.github.steveice10.mc.protocol.packet.login.client.LoginStartPacket;
import com.github.steveice10.packetlib.Client;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;

import ru.intervi.mcantibotproxy.server.FakeServer;

public class Proxy {
	public Proxy(FakeServer server) {
		this.server = server;
	}
	
	volatile FakeServer server;
	volatile HashMap<Session, Client> map = new HashMap<Session, Client>();
	
	public synchronized void put(Session session, boolean filter) {
		try {
			session.setReadTimeout(server.main.conf.normalReadTimeout);
			session.setWriteTimeout(server.main.conf.normalWriteTimeout);
			if (!filter) return;
			new Connector(getListener(), session, true).start();
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public Listener getListener() {
		return new Listener(this);
	}
	
	public class Listener extends SessionAdapter {
		public Listener(Proxy proxy) {
			PROXY = proxy;
		}
		
		public final Proxy PROXY;

		@Override
		public void packetReceived(PacketReceivedEvent event) {
			try {
				if (event.getPacket() instanceof HandshakePacket) return;
				else if (event.getPacket() instanceof LoginStartPacket) new Connector(this, event.getSession(), false).start();
				else {
					if (!event.getSession().hasFlag("authed")) return;
					if (map.containsKey(event.getSession())) map.get(event.getSession()).getSession().send(event.getPacket());
				}
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			}
		}
		
		@Override
		public void disconnected(DisconnectedEvent event) {
			try {
				if (map.containsKey(event.getSession())) map.get(event.getSession()).getSession().disconnect(event.getReason());
				map.remove(event.getSession());
			} catch(Exception e) {
				if (server.main.conf.debug) e.printStackTrace();
			}
		}
	}
	
	public void serverClosed() {
		try {
			map.clear();
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	public void serverStart() {
		
	}
}