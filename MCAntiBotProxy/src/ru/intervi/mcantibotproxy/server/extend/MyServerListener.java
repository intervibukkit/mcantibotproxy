package ru.intervi.mcantibotproxy.server.extend;

import java.math.BigInteger;
import java.net.Proxy;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.UUID;

import javax.crypto.SecretKey;

import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.auth.exception.request.RequestException;
import com.github.steveice10.mc.auth.service.SessionService;
import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.ServerListener;
import com.github.steveice10.mc.protocol.ServerLoginHandler;
import com.github.steveice10.mc.protocol.data.SubProtocol;
import com.github.steveice10.mc.protocol.packet.handshake.client.HandshakePacket;
import com.github.steveice10.mc.protocol.packet.login.client.EncryptionResponsePacket;
import com.github.steveice10.mc.protocol.packet.login.client.LoginStartPacket;
import com.github.steveice10.mc.protocol.packet.login.server.EncryptionRequestPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSetCompressionPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSuccessPacket;
import com.github.steveice10.mc.protocol.util.CryptUtil;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;

public class MyServerListener extends ServerListener {
	public MyServerListener(Session session) {
		serverId = session.getFlag("serverId");
	}
	
	private static final KeyPair KEY_PAIR = CryptUtil.generateKeyPair();
	private byte verifyToken[] = new byte[4];
	private String serverId = "";
	private String username = "";
	
	public void auth(Session session, SecretKey key) {
		boolean verify = session.hasFlag(MinecraftConstants.VERIFY_USERS_KEY) ? session.<Boolean>getFlag(MinecraftConstants.VERIFY_USERS_KEY) : true;
		GameProfile profile = null;
		if (verify && key != null) {
			Proxy proxy = session.<Proxy>getFlag(MinecraftConstants.AUTH_PROXY_KEY);
			if (proxy == null) {
				proxy = Proxy.NO_PROXY;
			}
			try {
				profile = new SessionService(proxy).getProfileByServer(username, new BigInteger(CryptUtil.getServerIdHash(serverId, KEY_PAIR.getPublic(), key)).toString(16));
			} catch(RequestException e) {
				session.disconnect("Failed to make session service request.", e);
				return;
			}
			if (profile == null) {
				session.disconnect("Failed to verify username.");
			}
		} else
			profile = new GameProfile(UUID.nameUUIDFromBytes(("OfflinePlayer:" + username).getBytes()), username);
		int threshold;
		if (session.hasFlag(MinecraftConstants.SERVER_COMPRESSION_THRESHOLD))
			threshold = session.getFlag(MinecraftConstants.SERVER_COMPRESSION_THRESHOLD);
		else threshold = 256;
		session.send(new LoginSetCompressionPacket(threshold));
		session.setCompressionThreshold(threshold);
		if (!((Boolean) session.getFlag("isNormal"))) session.send(new LoginSuccessPacket(profile));
		session.setFlag(MinecraftConstants.PROFILE_KEY, profile);
		Register.setSubProtocol(SubProtocol.GAME, false, session);
		ServerLoginHandler handler = session.getFlag(MinecraftConstants.SERVER_LOGIN_HANDLER_KEY);
		if (handler != null) handler.loggedIn(session);
	}
	
	@Override
	public void packetReceived(PacketReceivedEvent event) {
		Protocol protocol = (Protocol) event.getSession().getPacketProtocol();
		if (event.getPacket() instanceof HandshakePacket) {
			HandshakePacket hppacket = event.getPacket();
			switch(hppacket.getIntent()) {
				case STATUS:
					Register.setSubProtocol(SubProtocol.STATUS, false, event.getSession());
					break;
				case LOGIN:
					Register.setSubProtocol(SubProtocol.LOGIN, false, event.getSession());
					break;
				default:
					throw new UnsupportedOperationException("Invalid client intent: " + hppacket.getIntent());
			}
		} else if (event.getPacket() instanceof LoginStartPacket) {
			this.username = event.<LoginStartPacket>getPacket().getUsername();
			boolean verify = event.getSession().hasFlag(MinecraftConstants.VERIFY_USERS_KEY) ? event.getSession().<Boolean>getFlag(MinecraftConstants.VERIFY_USERS_KEY) : true;
			if (verify)
				event.getSession().send(new EncryptionRequestPacket(this.serverId, KEY_PAIR.getPublic(), this.verifyToken));
			else auth(event.getSession(), null);
		} else if (event.getPacket() instanceof EncryptionResponsePacket) {
			EncryptionResponsePacket erpacket = event.getPacket();
			PrivateKey privateKey = KEY_PAIR.getPrivate();
			if (!Arrays.equals(this.verifyToken, erpacket.getVerifyToken(privateKey))) {
				event.getSession().disconnect("Invalid nonce!");
				return;
			}
			SecretKey key = erpacket.getSecretKey(privateKey);
			protocol.enableEncryption(key);
			auth(event.getSession(), key);
		}
	}
}