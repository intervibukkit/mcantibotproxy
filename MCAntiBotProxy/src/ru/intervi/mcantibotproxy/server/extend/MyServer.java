package ru.intervi.mcantibotproxy.server.extend;

import com.github.steveice10.packetlib.Server;
import com.github.steveice10.packetlib.SessionFactory;
import com.github.steveice10.packetlib.packet.PacketProtocol;

public class MyServer extends Server {
	public MyServer(String host, int port, Class<? extends PacketProtocol> protocol, SessionFactory factory) {
		super(host, port, protocol, factory);
	}
	
	@Override
	public PacketProtocol createPacketProtocol() {
		return new Protocol();
	}
}