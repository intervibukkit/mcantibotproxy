package ru.intervi.mcantibotproxy.server.extend;

import java.math.BigInteger;
import java.net.Proxy;
import java.security.Key;

import javax.crypto.SecretKey;

import com.github.steveice10.mc.auth.data.GameProfile;
import com.github.steveice10.mc.auth.exception.request.InvalidCredentialsException;
import com.github.steveice10.mc.auth.exception.request.RequestException;
import com.github.steveice10.mc.auth.exception.request.ServiceUnavailableException;
import com.github.steveice10.mc.auth.service.SessionService;
import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.SubProtocol;
import com.github.steveice10.mc.protocol.data.handshake.HandshakeIntent;
import com.github.steveice10.mc.protocol.packet.handshake.client.HandshakePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerDisconnectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerSetCompressionPacket;
import com.github.steveice10.mc.protocol.packet.login.client.EncryptionResponsePacket;
import com.github.steveice10.mc.protocol.packet.login.client.LoginStartPacket;
import com.github.steveice10.mc.protocol.packet.login.server.EncryptionRequestPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginDisconnectPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSetCompressionPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSuccessPacket;
import com.github.steveice10.mc.protocol.util.CryptUtil;
import com.github.steveice10.packetlib.Client;
import com.github.steveice10.packetlib.Server;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.session.ConnectedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.google.gson.Gson;

public class Protocol extends MinecraftProtocol {
	public Protocol() {
		super("/");
	}
	
	public Protocol(GameProfile profile, String accessToken, Session session, int port) {
		super(profile, accessToken);
		clientSession = session;
		serverPort = port;
		GAMEPROFILE = profile;
		if (session.hasFlag("debug") && ((Boolean) session.getFlag("debug"))) debug = true;
	}
	
	private Session clientSession;
	private int serverPort;
	private GameProfile GAMEPROFILE;
	private boolean debug = false;
	
	public class MyClientListener extends SessionAdapter {
		@Override
		public void packetReceived(PacketReceivedEvent event) {
			try {
				Protocol protocol = (Protocol) event.getSession().getPacketProtocol();
				if (protocol.getSubProtocol() == SubProtocol.LOGIN) {
					if (event.getPacket() instanceof EncryptionRequestPacket) {
						EncryptionRequestPacket packet = event.getPacket();
						SecretKey key = CryptUtil.generateSharedKey();
						Proxy proxy = event.getSession().<Proxy>getFlag(MinecraftConstants.AUTH_PROXY_KEY);
						if (proxy == null) proxy = Proxy.NO_PROXY;
						GameProfile profile = event.getSession().getFlag(MinecraftConstants.PROFILE_KEY);
						String serverHash = new BigInteger(CryptUtil.getServerIdHash(packet.getServerId(), packet.getPublicKey(), key)).toString(16);
						String accessToken = event.getSession().getFlag(MinecraftConstants.ACCESS_TOKEN_KEY);
						try {
							new SessionService(proxy).joinServer(profile, accessToken, serverHash);
						} catch(ServiceUnavailableException e) {
							event.getSession().disconnect("Login failed: Authentication service unavailable.", e);
							return;
						} catch(InvalidCredentialsException e) {
							event.getSession().disconnect("Login failed: Invalid login session.", e);
							return;
						} catch(RequestException e) {
							event.getSession().disconnect("Login failed: Authentication error: " + e.getMessage(), e);
							return;
						}
						event.getSession().send(new EncryptionResponsePacket(key, packet.getPublicKey(), packet.getVerifyToken()));
						protocol.enableEncryption(key);
					} else if (event.getPacket() instanceof LoginSuccessPacket) {
						LoginSuccessPacket packet = event.getPacket();
						event.getSession().setFlag(MinecraftConstants.PROFILE_KEY, packet.getProfile());
						protocol.setSubProtocol(SubProtocol.GAME, true, event.getSession());
					} else if (event.getPacket() instanceof LoginDisconnectPacket) {
						LoginDisconnectPacket packet = event.getPacket();
						event.getSession().disconnect(packet.getReason().getFullText());
					} else if (event.getPacket() instanceof LoginSetCompressionPacket) {
						event.getSession().setCompressionThreshold(event.<LoginSetCompressionPacket>getPacket().getThreshold());
					}
				} else if (protocol.getSubProtocol() == SubProtocol.GAME) {
					if (event.getPacket() instanceof ServerDisconnectPacket) {
						event.getSession().disconnect(event.<ServerDisconnectPacket>getPacket().getReason().getFullText());
					} else if (event.getPacket() instanceof ServerSetCompressionPacket) {
						event.getSession().setCompressionThreshold(event.<ServerSetCompressionPacket>getPacket().getThreshold());
					}
				}
			} catch(Exception e) {
				if (debug) e.printStackTrace();
			}
		}
		
		private void sendHandshake (Session serverSession) {
			try {
				String local = clientSession.getLocalAddress().toString();
				local = local.substring(local.indexOf('/')+1, local.indexOf(':'));
				String host = local + "\00" + clientSession.getHost() + "\00" + GAMEPROFILE.getIdAsString();
				if (GAMEPROFILE != null && !GAMEPROFILE.getProperties().isEmpty()) host += "\00" + new Gson().toJson(GAMEPROFILE.getProperties());
				HandshakePacket handshake = new HandshakePacket(MinecraftConstants.PROTOCOL_VERSION, host, serverPort, HandshakeIntent.LOGIN);
				serverSession.send(handshake);
			} catch(Exception e) {
				if (debug) e.printStackTrace();
			}
		}
		
		@Override
		public void connected(ConnectedEvent event) {
			try {
				if (GAMEPROFILE == null) return;
				MinecraftProtocol protocol = (MinecraftProtocol) event.getSession().getPacketProtocol();
				if (protocol.getSubProtocol() == SubProtocol.LOGIN) {
					Register.setSubProtocol(SubProtocol.HANDSHAKE, true, event.getSession());
					sendHandshake(event.getSession());
					Register.setSubProtocol(SubProtocol.LOGIN, true, event.getSession());
					event.getSession().send(new LoginStartPacket(GAMEPROFILE.getName()));
				}
			} catch(Exception e) {
				if (debug) e.printStackTrace();
			}
		}
	}
	
	@Override
	public void newClientSession(Client client, Session session) {
		try {
			session.setFlag(MinecraftConstants.PROFILE_KEY, GAMEPROFILE);
			session.setFlag(MinecraftConstants.ACCESS_TOKEN_KEY, super.getAccessToken());
			Register.setSubProtocol(super.getSubProtocol(), true, session);
			session.addListener(new MyClientListener());
		} catch(Exception e) {
			if (debug) e.printStackTrace();
		}
	}
	
	@Override
	public void newServerSession(Server server, Session session) {
		try {
			super.setSubProtocol(SubProtocol.HANDSHAKE, false, session);
			session.addListener(new MyServerListener(session));
		} catch(Exception e) {
			if (debug) e.printStackTrace();
		}
	}
	
	@Override
	public void enableEncryption(Key key) {
		try {
			super.enableEncryption(key);
		} catch(Exception e) {
			if (debug) e.printStackTrace();
		}
	}
}