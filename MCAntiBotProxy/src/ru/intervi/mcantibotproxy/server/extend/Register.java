package ru.intervi.mcantibotproxy.server.extend;

import com.github.steveice10.mc.protocol.packet.ingame.client.ClientChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientKeepAlivePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientPluginMessagePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientRequestPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientResourcePackStatusPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientSettingsPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.ClientTabCompletePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerAbilitiesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerActionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerChangeHeldItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerInteractEntityPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerMovementPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerPlaceBlockPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerPositionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerStatePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerSwingArmPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.player.ClientPlayerUseItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientAdvancementTabPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientCloseWindowPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientConfirmTransactionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientCraftingBookDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientCreativeInventoryActionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientEnchantItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientPrepareCraftingGridPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.window.ClientWindowActionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientSpectatePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientSteerBoatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientSteerVehiclePacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientTeleportConfirmPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientUpdateSignPacket;
import com.github.steveice10.mc.protocol.packet.ingame.client.world.ClientVehicleMovePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerAdvancementTabPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerAdvancementsPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerBossBarPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerCombatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerDifficultyPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerDisconnectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerKeepAlivePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerPlayerListDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerPlayerListEntryPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerPluginMessagePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerResourcePackSendPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerRespawnPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerSetCooldownPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerStatisticsPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerSwitchCameraPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerTabCompletePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerTitlePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerUnlockRecipesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityAnimationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityAttachPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityCollectItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityDestroyPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityEffectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityEquipmentPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityHeadLookPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityMetadataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityMovementPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityPositionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityPropertiesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityRemoveEffectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntitySetPassengersPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityStatusPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityTeleportPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerEntityVelocityPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.ServerVehicleMovePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerAbilitiesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerChangeHeldItemPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerHealthPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerSetExperiencePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerUseBedPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnExpOrbPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnGlobalEntityPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnMobPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnObjectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnPaintingPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.spawn.ServerSpawnPlayerPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerCloseWindowPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerConfirmTransactionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerOpenWindowPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerSetSlotPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerWindowItemsPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerWindowPropertyPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerBlockBreakAnimPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerBlockChangePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerBlockValuePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerChunkDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerExplosionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerMapDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerMultiBlockChangePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerNotifyClientPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerOpenTileEntityEditorPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerPlayBuiltinSoundPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerPlayEffectPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerPlaySoundPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerSpawnParticlePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerSpawnPositionPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerUnloadChunkPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerUpdateTileEntityPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerUpdateTimePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerWorldBorderPacket;
import com.github.steveice10.mc.protocol.packet.login.client.LoginStartPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginDisconnectPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSetCompressionPacket;
import com.github.steveice10.mc.protocol.data.SubProtocol;
import com.github.steveice10.mc.protocol.packet.handshake.client.HandshakePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.scoreboard.ServerDisplayScoreboardPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.scoreboard.ServerScoreboardObjectivePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.scoreboard.ServerTeamPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.scoreboard.ServerUpdateScorePacket;
import com.github.steveice10.mc.protocol.packet.login.client.EncryptionResponsePacket;
import com.github.steveice10.mc.protocol.packet.login.server.EncryptionRequestPacket;
import com.github.steveice10.mc.protocol.packet.login.server.LoginSuccessPacket;
import com.github.steveice10.mc.protocol.packet.status.client.StatusPingPacket;
import com.github.steveice10.mc.protocol.packet.status.client.StatusQueryPacket;
import com.github.steveice10.mc.protocol.packet.status.server.StatusPongPacket;
import com.github.steveice10.mc.protocol.packet.status.server.StatusResponsePacket;
import com.github.steveice10.packetlib.Session;

public class Register {
	public static void setSubProtocol(SubProtocol subProtocol, boolean client, Session session) {
		switch (subProtocol) {
			case HANDSHAKE:
				if (client) {
					initClientHandshake(session);
				} else {
					initServerHandshake(session);
				}

				break;
			case LOGIN:
				if (client) {
					initClientLogin(session);
				} else {
					initServerLogin(session);
				}

				break;
			case GAME:
				if (client) {
					initClientGame(session);
				} else {
					initServerGame(session);
				}

				break;
			case STATUS:
				if (client) {
					initClientStatus(session);
				} else {
					initServerStatus(session);
				}

				break;
		}
	}

	public static void initClientHandshake(Session session) {
		session.getPacketProtocol().registerOutgoing(0, HandshakePacket.class);
	}

	private static void initServerHandshake(Session session) {
		session.getPacketProtocol().registerIncoming(0, HandshakePacket.class);
	}

	public static void initClientLogin(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, LoginDisconnectPacket.class);
		session.getPacketProtocol().registerIncoming(0x01, EncryptionRequestPacket.class);
		session.getPacketProtocol().registerIncoming(0x02, LoginSuccessPacket.class);
		session.getPacketProtocol().registerIncoming(0x03, LoginSetCompressionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x00, LoginStartPacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, EncryptionResponsePacket.class);
	}

	public static void initServerLogin(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, LoginStartPacket.class);
		session.getPacketProtocol().registerIncoming(0x01, EncryptionResponsePacket.class);

		session.getPacketProtocol().registerOutgoing(0x00, LoginDisconnectPacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, EncryptionRequestPacket.class);
		session.getPacketProtocol().registerOutgoing(0x02, LoginSuccessPacket.class);
		session.getPacketProtocol().registerOutgoing(0x03, LoginSetCompressionPacket.class);
	}

	public static void initClientGame(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, ServerSpawnObjectPacket.class);
		session.getPacketProtocol().registerIncoming(0x01, ServerSpawnExpOrbPacket.class);
		session.getPacketProtocol().registerIncoming(0x02, ServerSpawnGlobalEntityPacket.class);
		session.getPacketProtocol().registerIncoming(0x03, ServerSpawnMobPacket.class);
		session.getPacketProtocol().registerIncoming(0x04, ServerSpawnPaintingPacket.class);
		session.getPacketProtocol().registerIncoming(0x05, ServerSpawnPlayerPacket.class);
		session.getPacketProtocol().registerIncoming(0x06, ServerEntityAnimationPacket.class);
		session.getPacketProtocol().registerIncoming(0x07, ServerStatisticsPacket.class);
		session.getPacketProtocol().registerIncoming(0x08, ServerBlockBreakAnimPacket.class);
		session.getPacketProtocol().registerIncoming(0x09, ServerUpdateTileEntityPacket.class);
		session.getPacketProtocol().registerIncoming(0x0A, ServerBlockValuePacket.class);
		session.getPacketProtocol().registerIncoming(0x0B, ServerBlockChangePacket.class);
		session.getPacketProtocol().registerIncoming(0x0C, ServerBossBarPacket.class);
		session.getPacketProtocol().registerIncoming(0x0D, ServerDifficultyPacket.class);
		session.getPacketProtocol().registerIncoming(0x0E, ServerTabCompletePacket.class);
		session.getPacketProtocol().registerIncoming(0x0F, ServerChatPacket.class);
		session.getPacketProtocol().registerIncoming(0x10, ServerMultiBlockChangePacket.class);
		session.getPacketProtocol().registerIncoming(0x11, ServerConfirmTransactionPacket.class);
		session.getPacketProtocol().registerIncoming(0x12, ServerCloseWindowPacket.class);
		session.getPacketProtocol().registerIncoming(0x13, ServerOpenWindowPacket.class);
		session.getPacketProtocol().registerIncoming(0x14, ServerWindowItemsPacket.class);
		session.getPacketProtocol().registerIncoming(0x15, ServerWindowPropertyPacket.class);
		session.getPacketProtocol().registerIncoming(0x16, ServerSetSlotPacket.class);
		session.getPacketProtocol().registerIncoming(0x17, ServerSetCooldownPacket.class);
		session.getPacketProtocol().registerIncoming(0x18, ServerPluginMessagePacket.class);
		session.getPacketProtocol().registerIncoming(0x19, ServerPlaySoundPacket.class);
		session.getPacketProtocol().registerIncoming(0x1A, ServerDisconnectPacket.class);
		session.getPacketProtocol().registerIncoming(0x1B, ServerEntityStatusPacket.class);
		session.getPacketProtocol().registerIncoming(0x1C, ServerExplosionPacket.class);
		session.getPacketProtocol().registerIncoming(0x1D, ServerUnloadChunkPacket.class);
		session.getPacketProtocol().registerIncoming(0x1E, ServerNotifyClientPacket.class);
		session.getPacketProtocol().registerIncoming(0x1F, ServerKeepAlivePacket.class);
		session.getPacketProtocol().registerIncoming(0x20, ServerChunkDataPacket.class);
		session.getPacketProtocol().registerIncoming(0x21, ServerPlayEffectPacket.class);
		session.getPacketProtocol().registerIncoming(0x22, ServerSpawnParticlePacket.class);
		session.getPacketProtocol().registerIncoming(0x23, ServerJoinGamePacket.class);
		session.getPacketProtocol().registerIncoming(0x24, ServerMapDataPacket.class);
		session.getPacketProtocol().registerIncoming(0x25, ServerEntityMovementPacket.class);
		session.getPacketProtocol().registerIncoming(0x26, ServerEntityPositionPacket.class);
		session.getPacketProtocol().registerIncoming(0x27, ServerEntityPositionRotationPacket.class);
		session.getPacketProtocol().registerIncoming(0x28, ServerEntityRotationPacket.class);
		session.getPacketProtocol().registerIncoming(0x29, ServerVehicleMovePacket.class);
		session.getPacketProtocol().registerIncoming(0x2A, ServerOpenTileEntityEditorPacket.class);
		session.getPacketProtocol().registerIncoming(0x2B, ServerPlayerAbilitiesPacket.class);
		session.getPacketProtocol().registerIncoming(0x2C, ServerCombatPacket.class);
		session.getPacketProtocol().registerIncoming(0x2D, ServerPlayerListEntryPacket.class);
		session.getPacketProtocol().registerIncoming(0x2E, ServerPlayerPositionRotationPacket.class);
		session.getPacketProtocol().registerIncoming(0x2F, ServerPlayerUseBedPacket.class);
		session.getPacketProtocol().registerIncoming(0x30, ServerUnlockRecipesPacket.class);
		session.getPacketProtocol().registerIncoming(0x31, ServerEntityDestroyPacket.class);
		session.getPacketProtocol().registerIncoming(0x32, ServerEntityRemoveEffectPacket.class);
		session.getPacketProtocol().registerIncoming(0x33, ServerResourcePackSendPacket.class);
		session.getPacketProtocol().registerIncoming(0x34, ServerRespawnPacket.class);
		session.getPacketProtocol().registerIncoming(0x35, ServerEntityHeadLookPacket.class);
		session.getPacketProtocol().registerIncoming(0x36, ServerAdvancementTabPacket.class);
		session.getPacketProtocol().registerIncoming(0x37, ServerWorldBorderPacket.class);
		session.getPacketProtocol().registerIncoming(0x38, ServerSwitchCameraPacket.class);
		session.getPacketProtocol().registerIncoming(0x39, ServerPlayerChangeHeldItemPacket.class);
		session.getPacketProtocol().registerIncoming(0x3A, ServerDisplayScoreboardPacket.class);
		session.getPacketProtocol().registerIncoming(0x3B, ServerEntityMetadataPacket.class);
		session.getPacketProtocol().registerIncoming(0x3C, ServerEntityAttachPacket.class);
		session.getPacketProtocol().registerIncoming(0x3D, ServerEntityVelocityPacket.class);
		session.getPacketProtocol().registerIncoming(0x3E, ServerEntityEquipmentPacket.class);
		session.getPacketProtocol().registerIncoming(0x3F, ServerPlayerSetExperiencePacket.class);
		session.getPacketProtocol().registerIncoming(0x40, ServerPlayerHealthPacket.class);
		session.getPacketProtocol().registerIncoming(0x41, ServerScoreboardObjectivePacket.class);
		session.getPacketProtocol().registerIncoming(0x42, ServerEntitySetPassengersPacket.class);
		session.getPacketProtocol().registerIncoming(0x43, ServerTeamPacket.class);
		session.getPacketProtocol().registerIncoming(0x44, ServerUpdateScorePacket.class);
		session.getPacketProtocol().registerIncoming(0x45, ServerSpawnPositionPacket.class);
		session.getPacketProtocol().registerIncoming(0x46, ServerUpdateTimePacket.class);
		session.getPacketProtocol().registerIncoming(0x47, ServerTitlePacket.class);
		session.getPacketProtocol().registerIncoming(0x48, ServerPlayBuiltinSoundPacket.class);
		session.getPacketProtocol().registerIncoming(0x49, ServerPlayerListDataPacket.class);
		session.getPacketProtocol().registerIncoming(0x4A, ServerEntityCollectItemPacket.class);
		session.getPacketProtocol().registerIncoming(0x4B, ServerEntityTeleportPacket.class);
		session.getPacketProtocol().registerIncoming(0x4C, ServerAdvancementsPacket.class);
		session.getPacketProtocol().registerIncoming(0x4D, ServerEntityPropertiesPacket.class);
		session.getPacketProtocol().registerIncoming(0x4E, ServerEntityEffectPacket.class);

		session.getPacketProtocol().registerOutgoing(0x00, ClientTeleportConfirmPacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, ClientPrepareCraftingGridPacket.class);
		session.getPacketProtocol().registerOutgoing(0x02, ClientTabCompletePacket.class);
		session.getPacketProtocol().registerOutgoing(0x03, ClientChatPacket.class);
		session.getPacketProtocol().registerOutgoing(0x04, ClientRequestPacket.class);
		session.getPacketProtocol().registerOutgoing(0x05, ClientSettingsPacket.class);
		session.getPacketProtocol().registerOutgoing(0x06, ClientConfirmTransactionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x07, ClientEnchantItemPacket.class);
		session.getPacketProtocol().registerOutgoing(0x08, ClientWindowActionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x09, ClientCloseWindowPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0A, ClientPluginMessagePacket.class);
		session.getPacketProtocol().registerOutgoing(0x0B, ClientPlayerInteractEntityPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0C, ClientKeepAlivePacket.class);
		session.getPacketProtocol().registerOutgoing(0x0D, ClientPlayerMovementPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0E, ClientPlayerPositionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0F, ClientPlayerPositionRotationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x10, ClientPlayerRotationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x11, ClientVehicleMovePacket.class);
		session.getPacketProtocol().registerOutgoing(0x12, ClientSteerBoatPacket.class);
		session.getPacketProtocol().registerOutgoing(0x13, ClientPlayerAbilitiesPacket.class);
		session.getPacketProtocol().registerOutgoing(0x14, ClientPlayerActionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x15, ClientPlayerStatePacket.class);
		session.getPacketProtocol().registerOutgoing(0x16, ClientSteerVehiclePacket.class);
		session.getPacketProtocol().registerOutgoing(0x17, ClientCraftingBookDataPacket.class);
		session.getPacketProtocol().registerOutgoing(0x18, ClientResourcePackStatusPacket.class);
		session.getPacketProtocol().registerOutgoing(0x19, ClientAdvancementTabPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1A, ClientPlayerChangeHeldItemPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1B, ClientCreativeInventoryActionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1C, ClientUpdateSignPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1D, ClientPlayerSwingArmPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1E, ClientSpectatePacket.class);
		session.getPacketProtocol().registerOutgoing(0x1F, ClientPlayerPlaceBlockPacket.class);
		session.getPacketProtocol().registerOutgoing(0x20, ClientPlayerUseItemPacket.class);
	}

	public static void initServerGame(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, ClientTeleportConfirmPacket.class);
		session.getPacketProtocol().registerIncoming(0x01, ClientPrepareCraftingGridPacket.class);
		session.getPacketProtocol().registerIncoming(0x02, ClientTabCompletePacket.class);
		session.getPacketProtocol().registerIncoming(0x03, ClientChatPacket.class);
		session.getPacketProtocol().registerIncoming(0x04, ClientRequestPacket.class);
		session.getPacketProtocol().registerIncoming(0x05, ClientSettingsPacket.class);
		session.getPacketProtocol().registerIncoming(0x06, ClientConfirmTransactionPacket.class);
		session.getPacketProtocol().registerIncoming(0x07, ClientEnchantItemPacket.class);
		session.getPacketProtocol().registerIncoming(0x08, ClientWindowActionPacket.class);
		session.getPacketProtocol().registerIncoming(0x09, ClientCloseWindowPacket.class);
		session.getPacketProtocol().registerIncoming(0x0A, ClientPluginMessagePacket.class);
		session.getPacketProtocol().registerIncoming(0x0B, ClientPlayerInteractEntityPacket.class);
		session.getPacketProtocol().registerIncoming(0x0C, ClientKeepAlivePacket.class);
		session.getPacketProtocol().registerIncoming(0x0D, ClientPlayerMovementPacket.class);
		session.getPacketProtocol().registerIncoming(0x0E, ClientPlayerPositionPacket.class);
		session.getPacketProtocol().registerIncoming(0x0F, ClientPlayerPositionRotationPacket.class);
		session.getPacketProtocol().registerIncoming(0x10, ClientPlayerRotationPacket.class);
		session.getPacketProtocol().registerIncoming(0x11, ClientVehicleMovePacket.class);
		session.getPacketProtocol().registerIncoming(0x12, ClientSteerBoatPacket.class);
		session.getPacketProtocol().registerIncoming(0x13, ClientPlayerAbilitiesPacket.class);
		session.getPacketProtocol().registerIncoming(0x14, ClientPlayerActionPacket.class);
		session.getPacketProtocol().registerIncoming(0x15, ClientPlayerStatePacket.class);
		session.getPacketProtocol().registerIncoming(0x16, ClientSteerVehiclePacket.class);
		session.getPacketProtocol().registerIncoming(0x17, ClientCraftingBookDataPacket.class);
		session.getPacketProtocol().registerIncoming(0x18, ClientResourcePackStatusPacket.class);
		session.getPacketProtocol().registerIncoming(0x19, ClientAdvancementTabPacket.class);
		session.getPacketProtocol().registerIncoming(0x1A, ClientPlayerChangeHeldItemPacket.class);
		session.getPacketProtocol().registerIncoming(0x1B, ClientCreativeInventoryActionPacket.class);
		session.getPacketProtocol().registerIncoming(0x1C, ClientUpdateSignPacket.class);
		session.getPacketProtocol().registerIncoming(0x1D, ClientPlayerSwingArmPacket.class);
		session.getPacketProtocol().registerIncoming(0x1E, ClientSpectatePacket.class);
		session.getPacketProtocol().registerIncoming(0x1F, ClientPlayerPlaceBlockPacket.class);
		session.getPacketProtocol().registerIncoming(0x20, ClientPlayerUseItemPacket.class);

		session.getPacketProtocol().registerOutgoing(0x00, ServerSpawnObjectPacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, ServerSpawnExpOrbPacket.class);
		session.getPacketProtocol().registerOutgoing(0x02, ServerSpawnGlobalEntityPacket.class);
		session.getPacketProtocol().registerOutgoing(0x03, ServerSpawnMobPacket.class);
		session.getPacketProtocol().registerOutgoing(0x04, ServerSpawnPaintingPacket.class);
		session.getPacketProtocol().registerOutgoing(0x05, ServerSpawnPlayerPacket.class);
		session.getPacketProtocol().registerOutgoing(0x06, ServerEntityAnimationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x07, ServerStatisticsPacket.class);
		session.getPacketProtocol().registerOutgoing(0x08, ServerBlockBreakAnimPacket.class);
		session.getPacketProtocol().registerOutgoing(0x09, ServerUpdateTileEntityPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0A, ServerBlockValuePacket.class);
		session.getPacketProtocol().registerOutgoing(0x0B, ServerBlockChangePacket.class);
		session.getPacketProtocol().registerOutgoing(0x0C, ServerBossBarPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0D, ServerDifficultyPacket.class);
		session.getPacketProtocol().registerOutgoing(0x0E, ServerTabCompletePacket.class);
		session.getPacketProtocol().registerOutgoing(0x0F, ServerChatPacket.class);
		session.getPacketProtocol().registerOutgoing(0x10, ServerMultiBlockChangePacket.class);
		session.getPacketProtocol().registerOutgoing(0x11, ServerConfirmTransactionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x12, ServerCloseWindowPacket.class);
		session.getPacketProtocol().registerOutgoing(0x13, ServerOpenWindowPacket.class);
		session.getPacketProtocol().registerOutgoing(0x14, ServerWindowItemsPacket.class);
		session.getPacketProtocol().registerOutgoing(0x15, ServerWindowPropertyPacket.class);
		session.getPacketProtocol().registerOutgoing(0x16, ServerSetSlotPacket.class);
		session.getPacketProtocol().registerOutgoing(0x17, ServerSetCooldownPacket.class);
		session.getPacketProtocol().registerOutgoing(0x18, ServerPluginMessagePacket.class);
		session.getPacketProtocol().registerOutgoing(0x19, ServerPlaySoundPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1A, ServerDisconnectPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1B, ServerEntityStatusPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1C, ServerExplosionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1D, ServerUnloadChunkPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1E, ServerNotifyClientPacket.class);
		session.getPacketProtocol().registerOutgoing(0x1F, ServerKeepAlivePacket.class);
		session.getPacketProtocol().registerOutgoing(0x20, ServerChunkDataPacket.class);
		session.getPacketProtocol().registerOutgoing(0x21, ServerPlayEffectPacket.class);
		session.getPacketProtocol().registerOutgoing(0x22, ServerSpawnParticlePacket.class);
		session.getPacketProtocol().registerOutgoing(0x23, ServerJoinGamePacket.class);
		session.getPacketProtocol().registerOutgoing(0x24, ServerMapDataPacket.class);
		session.getPacketProtocol().registerOutgoing(0x25, ServerEntityMovementPacket.class);
		session.getPacketProtocol().registerOutgoing(0x26, ServerEntityPositionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x27, ServerEntityPositionRotationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x28, ServerEntityRotationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x29, ServerVehicleMovePacket.class);
		session.getPacketProtocol().registerOutgoing(0x2A, ServerOpenTileEntityEditorPacket.class);
		session.getPacketProtocol().registerOutgoing(0x2B, ServerPlayerAbilitiesPacket.class);
		session.getPacketProtocol().registerOutgoing(0x2C, ServerCombatPacket.class);
		session.getPacketProtocol().registerOutgoing(0x2D, ServerPlayerListEntryPacket.class);
		session.getPacketProtocol().registerOutgoing(0x2E, ServerPlayerPositionRotationPacket.class);
		session.getPacketProtocol().registerOutgoing(0x2F, ServerPlayerUseBedPacket.class);
		session.getPacketProtocol().registerOutgoing(0x30, ServerUnlockRecipesPacket.class);
		session.getPacketProtocol().registerOutgoing(0x31, ServerEntityDestroyPacket.class);
		session.getPacketProtocol().registerOutgoing(0x32, ServerEntityRemoveEffectPacket.class);
		session.getPacketProtocol().registerOutgoing(0x33, ServerResourcePackSendPacket.class);
		session.getPacketProtocol().registerOutgoing(0x34, ServerRespawnPacket.class);
		session.getPacketProtocol().registerOutgoing(0x35, ServerEntityHeadLookPacket.class);
		session.getPacketProtocol().registerOutgoing(0x36, ServerAdvancementTabPacket.class);
		session.getPacketProtocol().registerOutgoing(0x37, ServerWorldBorderPacket.class);
		session.getPacketProtocol().registerOutgoing(0x38, ServerSwitchCameraPacket.class);
		session.getPacketProtocol().registerOutgoing(0x39, ServerPlayerChangeHeldItemPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3A, ServerDisplayScoreboardPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3B, ServerEntityMetadataPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3C, ServerEntityAttachPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3D, ServerEntityVelocityPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3E, ServerEntityEquipmentPacket.class);
		session.getPacketProtocol().registerOutgoing(0x3F, ServerPlayerSetExperiencePacket.class);
		session.getPacketProtocol().registerOutgoing(0x40, ServerPlayerHealthPacket.class);
		session.getPacketProtocol().registerOutgoing(0x41, ServerScoreboardObjectivePacket.class);
		session.getPacketProtocol().registerOutgoing(0x42, ServerEntitySetPassengersPacket.class);
		session.getPacketProtocol().registerOutgoing(0x43, ServerTeamPacket.class);
		session.getPacketProtocol().registerOutgoing(0x44, ServerUpdateScorePacket.class);
		session.getPacketProtocol().registerOutgoing(0x45, ServerSpawnPositionPacket.class);
		session.getPacketProtocol().registerOutgoing(0x46, ServerUpdateTimePacket.class);
		session.getPacketProtocol().registerOutgoing(0x47, ServerTitlePacket.class);
		session.getPacketProtocol().registerOutgoing(0x48, ServerPlayBuiltinSoundPacket.class);
		session.getPacketProtocol().registerOutgoing(0x49, ServerPlayerListDataPacket.class);
		session.getPacketProtocol().registerOutgoing(0x4A, ServerEntityCollectItemPacket.class);
		session.getPacketProtocol().registerOutgoing(0x4B, ServerEntityTeleportPacket.class);
		session.getPacketProtocol().registerOutgoing(0x4C, ServerAdvancementsPacket.class);
		session.getPacketProtocol().registerOutgoing(0x4D, ServerEntityPropertiesPacket.class);
		session.getPacketProtocol().registerOutgoing(0x4E, ServerEntityEffectPacket.class);
	}

	public static void initClientStatus(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, StatusResponsePacket.class);
		session.getPacketProtocol().registerIncoming(0x01, StatusPongPacket.class);
		session.getPacketProtocol().registerOutgoing(0x00, StatusQueryPacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, StatusPingPacket.class);
	}

	public static void initServerStatus(Session session) {
		session.getPacketProtocol().registerIncoming(0x00, StatusQueryPacket.class);
		session.getPacketProtocol().registerIncoming(0x01, StatusPingPacket.class);
		session.getPacketProtocol().registerOutgoing(0x00, StatusResponsePacket.class);
		session.getPacketProtocol().registerOutgoing(0x01, StatusPongPacket.class);
	}
}