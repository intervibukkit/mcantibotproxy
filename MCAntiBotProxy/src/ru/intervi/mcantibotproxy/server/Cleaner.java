package ru.intervi.mcantibotproxy.server;

import java.util.ArrayList;
import java.util.TimerTask;

import com.github.steveice10.packetlib.Session;

public class Cleaner extends TimerTask {
	public Cleaner(FakeServer server) {
		this.server = server;
	}
	
	private FakeServer server;
	
	@Override
	public void run() {
		try {
			ArrayList<Session> remove = new ArrayList<Session>();
			long stamp = System.currentTimeMillis();
			for (Session session : server.server.getSessions()) {
				if (((Boolean) session.getFlag("isNormal"))) continue;
				if ((stamp - ((Long) session.getFlag("timestamp")).longValue()) >= server.main.conf.waitMS) remove.add(session);
			}
			for (Session session : remove) {
				try {
					if (server.main.conf.debug) System.out.println(session.getHost() + " timeout, disconnect");
					session.disconnect(server.main.conf.timeoutMsg);
				} catch(Exception e) {
					if (server.main.conf.debug) e.printStackTrace();
				}
			}
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
}