package ru.intervi.mcantibotproxy.server;

import com.github.steveice10.mc.protocol.data.status.ServerStatusInfo;
import com.github.steveice10.mc.protocol.data.status.handler.ServerInfoBuilder;
import com.github.steveice10.packetlib.Session;

public class ServerInfo implements ServerInfoBuilder {
	public ServerInfo(FakeServer server) {
		this.server = server;
	}
	
	private FakeServer server;
	
	@Override
	public ServerStatusInfo buildInfo(Session session) {
		if (server.main.conf.debug) {
			System.out.println(session.getHost() + " get info");
			if (server.pinger.status != null) System.out.println("-- info: " + server.pinger.status.toString());
		}
		return server.pinger.status;
	}
}