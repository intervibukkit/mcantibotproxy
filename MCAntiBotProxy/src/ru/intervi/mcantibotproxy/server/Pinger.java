package ru.intervi.mcantibotproxy.server;

import java.util.TimerTask;

import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.MinecraftProtocol;
import com.github.steveice10.mc.protocol.data.SubProtocol;
import com.github.steveice10.mc.protocol.data.status.ServerStatusInfo;
import com.github.steveice10.mc.protocol.packet.status.server.StatusResponsePacket;
import com.github.steveice10.packetlib.Client;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;
import com.github.steveice10.packetlib.tcp.TcpSessionFactory;

import ru.intervi.mcantibotproxy.Config;

public class Pinger extends TimerTask {
	public Pinger(Config conf) {
		this.conf = conf;
	}
	
	private Config conf;
	public volatile ServerStatusInfo status = null;
	private Client client;
	
	public class Listener extends SessionAdapter {
		@Override
		public void packetReceived(PacketReceivedEvent event) {
			try {
				if (event.getPacket() instanceof StatusResponsePacket) {
					status = ((StatusResponsePacket) event.getPacket()).getInfo();
					client.getSession().disconnect("info saving, quit");
				}
			} catch(Exception e) {
				status = null;
				if (conf.debug) e.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
		try {
			MinecraftProtocol protocol = new MinecraftProtocol(SubProtocol.STATUS);
			client = new Client(conf.serverHost, conf.serverPort, protocol, new TcpSessionFactory(conf.proxy));
			client.getSession().setFlag(MinecraftConstants.AUTH_PROXY_KEY, conf.authProxy);
			client.getSession().addListener(new Listener());
			client.getSession().setReadTimeout(conf.normalReadTimeout);
			client.getSession().setWriteTimeout(conf.normalWriteTimeout);
			client.getSession().connect();
			if (client.getSession().isConnected()) while(client.getSession().isConnected()) Thread.sleep(1);
			else status = null;
		} catch(Exception e) {
			status = null;
			if (conf.debug) e.printStackTrace();
		}
	}
}