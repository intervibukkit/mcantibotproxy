package ru.intervi.mcantibotproxy.server;

import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.mc.protocol.data.handshake.HandshakeIntent;
import com.github.steveice10.mc.protocol.packet.handshake.client.HandshakePacket;
import com.github.steveice10.mc.protocol.packet.status.client.StatusPingPacket;
import com.github.steveice10.mc.protocol.packet.status.client.StatusQueryPacket;
import com.github.steveice10.mc.protocol.packet.status.server.StatusPongPacket;
import com.github.steveice10.mc.protocol.packet.status.server.StatusResponsePacket;
import com.github.steveice10.packetlib.event.session.ConnectedEvent;
import com.github.steveice10.packetlib.event.session.DisconnectedEvent;
import com.github.steveice10.packetlib.event.session.PacketReceivedEvent;
import com.github.steveice10.packetlib.event.session.PacketSentEvent;
import com.github.steveice10.packetlib.event.session.SessionAdapter;

public class SessionListener extends SessionAdapter {
	public SessionListener(FakeServer server) {
		this.server = server;
	}
	
	private FakeServer server;
	
	@Override
	public void packetReceived(PacketReceivedEvent event) {
		try {
			if (server.main.conf.debug) {
				System.out.println(event.getSession().getHost() + " packet received");
				System.out.println("-- packet: " + event.getPacket().toString());
			}
			if (event.getPacket() instanceof HandshakePacket) {
				if (((HandshakePacket) event.getPacket()).getIntent() == HandshakeIntent.LOGIN) {
					if (!server.listener.isAttack() && !server.main.conf.forceCheck) {
						server.putToProxy(event.getSession(), false);
						if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " put to proxy");
					} else {
						server.putToFilter(event.getSession());
						if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " put to map filter");
					}
				} else if (server.pinger.status == null) event.getSession().disconnect("server closed");
			}
			else if (event.getPacket() instanceof StatusQueryPacket)
				event.getSession().send(new StatusResponsePacket(server.pinger.status));
			else if (event.getPacket() instanceof StatusPingPacket)
				event.getSession().send(new StatusPongPacket(System.currentTimeMillis()));
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void packetSent(PacketSentEvent event) {
		if (server.main.conf.debug) {
			System.out.println(event.getSession().getHost() + " packet sent");
			System.out.println("-- packet: " + event.getPacket().toString());
		}
	}
	
	@Override
	public void connected(ConnectedEvent event) {
		try {
			if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " connected");
			event.getSession().setFlag(MinecraftConstants.PING_KEY, 0);
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void disconnected(DisconnectedEvent event) {
		try {
			if (server.main.conf.debug)
				System.out.println(event.getSession().getHost() + " disconnected: " + event.getReason());
			server.server.removeSession(event.getSession());
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
}