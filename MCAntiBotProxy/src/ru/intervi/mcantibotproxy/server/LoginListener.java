package ru.intervi.mcantibotproxy.server;

import com.github.steveice10.mc.protocol.ServerLoginHandler;
import com.github.steveice10.mc.protocol.data.game.chunk.Chunk;
import com.github.steveice10.mc.protocol.data.game.chunk.Column;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.ItemStack;
import com.github.steveice10.mc.protocol.data.game.entity.metadata.Position;
import com.github.steveice10.mc.protocol.data.game.entity.player.GameMode;
import com.github.steveice10.mc.protocol.data.game.entity.player.PositionElement;
import com.github.steveice10.mc.protocol.data.game.setting.Difficulty;
import com.github.steveice10.mc.protocol.data.game.window.WindowType;
import com.github.steveice10.mc.protocol.data.game.world.WorldType;
import com.github.steveice10.mc.protocol.data.game.world.map.MapData;
import com.github.steveice10.mc.protocol.data.game.world.map.MapIcon;
import com.github.steveice10.mc.protocol.data.message.Message;
import com.github.steveice10.mc.protocol.data.message.TextMessage;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerChatPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerDifficultyPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.ServerJoinGamePacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerAbilitiesPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.entity.player.ServerPlayerPositionRotationPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.window.ServerSetSlotPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerChunkDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerMapDataPacket;
import com.github.steveice10.mc.protocol.packet.ingame.server.world.ServerSpawnPositionPacket;
import com.github.steveice10.packetlib.Session;

import ru.intervi.mcantibotproxy.filter.FilterData;

public class LoginListener implements ServerLoginHandler {
	public LoginListener(FakeServer server) {
		this.server = server;
	}
	
	private FakeServer server;
	
	public final ServerJoinGamePacket JOINPACKET = new ServerJoinGamePacket(0, false, GameMode.SURVIVAL, 0, Difficulty.PEACEFUL, 10, WorldType.DEFAULT, false);
	public final ServerDifficultyPacket DIFFPACKET = new ServerDifficultyPacket(Difficulty.PEACEFUL);
	public final ServerPlayerAbilitiesPacket ABIPACKET = new ServerPlayerAbilitiesPacket(true, true, true, false, 0, 0);
	public final ServerSpawnPositionPacket POSITIONPACKET = new ServerSpawnPositionPacket(new Position(0, 0, 0));
	public final ServerPlayerPositionRotationPacket ROTATIONPACKET = new ServerPlayerPositionRotationPacket(0, 150, 0, 90, 90, 0, new PositionElement[0]);
	public final ServerChunkDataPacket CHUNKPACKET = getEmptyChunk();
	public final ServerSetSlotPacket SETSLOTPACKET = new ServerSetSlotPacket(WindowType.GENERIC_INVENTORY.ordinal(), 36, new ItemStack(358));
	
	public static ServerChunkDataPacket getEmptyChunk() {
		Chunk[] chunks = new Chunk[16];
		for (int i = 0; i < chunks.length; i++) chunks[i] = new Chunk(true);
		return new ServerChunkDataPacket(new Column(0, 0, chunks, new byte[256], null));
	}
	
	@Override
	public void loggedIn(Session session) {
		try {
			if (((Boolean) session.getFlag("isNormal"))) return;
			if (server.main.conf.debug) System.out.println(session.getHost() + " logged in");
			session.send(JOINPACKET);
			session.send(DIFFPACKET);
			session.send(ABIPACKET);
			session.send(POSITIONPACKET);
			session.send(ROTATIONPACKET);
			session.send(CHUNKPACKET);
			session.send(SETSLOTPACKET);
			MapIcon[] mi = {};
			FilterData data = server.filter.getData(session);
			session.send(new ServerMapDataPacket(0, (byte) 1, false, mi, new MapData(128, 128, 0, 0, data.MAP)));
			Message mess = new TextMessage(data.TEXT[0]);
			for (int i = 1; i < data.TEXT.length; i++) mess.addExtra(new TextMessage('\n' + data.TEXT[i]));
			session.send(new ServerChatPacket(mess));
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
}