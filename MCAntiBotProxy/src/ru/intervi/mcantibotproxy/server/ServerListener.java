package ru.intervi.mcantibotproxy.server;

import java.util.logging.Logger;

import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.event.server.ServerAdapter;
import com.github.steveice10.packetlib.event.server.ServerClosedEvent;
import com.github.steveice10.packetlib.event.server.SessionAddedEvent;
import com.github.steveice10.packetlib.event.server.SessionRemovedEvent;

import ru.intervi.mcantibotproxy.stat.Stat;

public class ServerListener extends ServerAdapter {
	public ServerListener(FakeServer server) {
		this.server = server;
		stat = new Stat(this.server);
	}
	
	private FakeServer server;
	private int logins = 0;
	private long oldStamp = System.currentTimeMillis();
	private long changeStamp = 0;
	private boolean attack = false;
	private Stat stat;
	
	public boolean isAttack() {
		return attack;
	}
	
	public int getLogins() {
		return logins;
	}
	
	@Override
	public void sessionAdded(SessionAddedEvent event) {
		try {
			stat.login();
			logins++;
			if (server.server.getSessions().size() >= server.main.conf.sessionLimit) {
				if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " kick, session limit");
				event.getSession().disconnect(server.main.conf.kickLimit);
				return;
			}
			if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " session added");
			long timeStamp = System.currentTimeMillis();
			if ((timeStamp - oldStamp) >= server.main.conf.loginMS) {
				oldStamp = timeStamp;
				logins = 1;
			}
			if (logins > server.main.conf.loginLimit) {
				changeStamp = timeStamp;
				if (!attack) {
					attack = true;
					stat.setAttack(true);
					Logger.getLogger("MCAntiBotProxy").warning("start attack");
				}
			} else if (attack && (timeStamp - changeStamp) >= server.main.conf.waitMS) {
				attack = false;
				stat.setAttack(false);
				Logger.getLogger("MCAntiBotProxy").warning("stop attack");
			}
			if (attack && server.main.conf.ipLimit > 0) {
				String host = event.getSession().getHost();
				int sc = 0;
				for (Session s : server.server.getSessions()) {
					if (s.getHost().equals(host)) sc++;
					if (sc >= server.main.conf.ipLimit) {
						event.getSession().disconnect(server.main.conf.ipMsg);
						if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " ip limit, disconnect");
						return;
					}
				}
			}
			event.getSession().setFlag("timestamp", Long.valueOf(System.currentTimeMillis()));
			event.getSession().addListener(server.getSessionListener());
			event.getSession().setReadTimeout(attack || server.main.conf.forceCheck ? server.main.conf.attackReadTimeout : server.main.conf.normalReadTimeout);
			event.getSession().setWriteTimeout(attack || server.main.conf.forceCheck ? server.main.conf.attackWriteTimeout : server.main.conf.normalWriteTimeout);
			if (!attack && !server.main.conf.forceCheck) event.getSession().setFlag("isNormal", Boolean.valueOf(true));
			else event.getSession().setFlag("isNormal", Boolean.valueOf(false));
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void sessionRemoved(SessionRemovedEvent event) {
		try {
			if (server.main.conf.debug) System.out.println(event.getSession().getHost() + " session removed");
		} catch(Exception e) {
			if (server.main.conf.debug) e.printStackTrace();
		}
	}
	
	@Override
	public void serverClosed(ServerClosedEvent event) {
		try {
			server.proxy.serverClosed();
			server.filter.serverClosed();
			stat.serverClosed();
			if (server.main.conf.debug) System.out.println("server closed");
		} catch(Exception e) {e.printStackTrace();}
	}
}