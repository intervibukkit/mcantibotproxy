package ru.intervi.mcantibotproxy.server;

import java.util.Timer;

import com.github.steveice10.mc.protocol.MinecraftConstants;
import com.github.steveice10.packetlib.Session;
import com.github.steveice10.packetlib.tcp.TcpSessionFactory;

import ru.intervi.mcantibotproxy.Main;
import ru.intervi.mcantibotproxy.filter.Filter;
import ru.intervi.mcantibotproxy.proxy.Proxy;
import ru.intervi.mcantibotproxy.server.extend.MyServer;
import ru.intervi.mcantibotproxy.server.extend.Protocol;

public class FakeServer {
	public FakeServer(Main main) {
		this.main = main;
		timer = new Timer();
		pinger = new Pinger(main.conf);
		listener = new ServerListener(this);
		server = new MyServer(main.conf.host, main.conf.port, Protocol.class, new TcpSessionFactory(main.conf.proxy));
		filter = new Filter(this);
		proxy = new Proxy(this);
		server.setGlobalFlag("serverId", main.conf.serverId);
		server.setGlobalFlag(MinecraftConstants.AUTH_PROXY_KEY, main.conf.authProxy);
		server.setGlobalFlag(MinecraftConstants.VERIFY_USERS_KEY, main.conf.onlineMode);
		server.setGlobalFlag(MinecraftConstants.SERVER_COMPRESSION_THRESHOLD, main.conf.compression);
		server.setGlobalFlag(MinecraftConstants.SERVER_INFO_BUILDER_KEY, new ServerInfo(this));
		server.setGlobalFlag(MinecraftConstants.SERVER_LOGIN_HANDLER_KEY, new LoginListener(this));
		server.setGlobalFlag(MinecraftConstants.ACCESS_TOKEN_KEY, main.conf.onlineMode);
		server.setGlobalFlag(MinecraftConstants.SERVER_PING_TIME_HANDLER_KEY, 0);
		server.addListener(listener);
	}
	
	public volatile Main main;
	private Timer timer;
	public volatile Pinger pinger;
	public volatile ServerListener listener;
	public volatile MyServer server;
	public volatile Filter filter;
	public volatile Proxy proxy;
	
	public void start() {
		try {
			proxy.serverStart();
			filter.serverStart();
			timer.schedule(pinger, 0, main.conf.statusPingMS);
			timer.schedule(new Cleaner(this), main.conf.tlClearMS, main.conf.tlClearMS);
			server.bind();
		} catch(Exception e) {
			if (main.conf.debug) e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			timer.cancel();
			server.close();
			timer = new Timer();
			pinger = new Pinger(main.conf);
		} catch(Exception e) {
			if (main.conf.debug) e.printStackTrace();
		}
	}
	
	public synchronized void putToProxy(Session session, boolean filter) {
		try {
			session.setFlag("isNormal", Boolean.valueOf(true));
			session.addListener(proxy.getListener());
			proxy.put(session, filter);
		} catch(Exception e) {
			if (main.conf.debug) e.printStackTrace();
		}
	}
	
	public synchronized void putToFilter(Session session) {
		try {
			session.addListener(filter.getListener());
		} catch(Exception e) {
			if (main.conf.debug) e.printStackTrace();
		}
	}
	
	public void banWrong(Session session) {
		try {
			if (!main.conf.ban) {
				session.disconnect(main.conf.kickMsg);
				return;
			}
			if (main.conf.debug) System.out.println(session.getHost() + " ban wrong or timeout");
			Runtime.getRuntime().exec(main.conf.bancmd.replaceAll("%host%", session.getHost()));
			session.disconnect(main.conf.banMsg);
		} catch(Exception e) {
			if (main.conf.debug) e.printStackTrace();
		}
	}
	
	public SessionListener getSessionListener() {
		return new SessionListener(this);
	}
}