package ru.intervi.mcantibotproxy;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.imageio.ImageIO;

import ru.intervi.mcantibotproxy.filter.Map;

public class Images {
	public Images(Main main) {
		this.main = main;
		load();
	}
	
	private Main main;
	private volatile HashMap<String, byte[]> map = new HashMap<String, byte[]>();
	
	public byte[] get(String key) {
		return map.get(key);
	}
	
	public List<String> getKeys() {
		return new ArrayList<String>(map.keySet());
	}
	
	public void load() {
		try {
			File file = new File(main.PATH.getAbsolutePath() + File.separatorChar + "images.properties");
			if (!file.isFile()) Config.saveFile(this.getClass().getResourceAsStream("/images.properties"), file);
			Properties prop = new Properties();
			prop.load(new FileReader(file));
			File dir = new File(main.PATH.getAbsolutePath() + File.separatorChar + "images");
			if (!dir.isDirectory()) {
				dir.mkdirs();
				String[] images = {
						"arrow.png", "circle.png", "equal.png", "line.png", "octothorpe.png", "percent.png", "plus.png",
						"squard.png", "text.png", "triangle.png"
						};
				for (String img : images) {
					File wfile = new File(main.PATH.getAbsolutePath() + File.separatorChar + "images" + File.separatorChar + img);
					Config.saveFile(this.getClass().getResourceAsStream("/images/" + img), wfile);
				}
			}
			for (String key : prop.stringPropertyNames()) {
				File fimg = new File(dir.getAbsolutePath() + File.separatorChar + prop.getProperty(key));
				if (!fimg.isFile()) continue;
				map.put(key, new Map().drawImage(0, 0, ImageIO.read(fimg)));
			}
		} catch(Exception e) {e.printStackTrace();}
	}
}